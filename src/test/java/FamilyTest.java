import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;


public class FamilyTest {
    private Human mother1 = new Human("Saida", "Salimova");
    private Human father1 = new Human("Mammad", "Salimov");

    private Family family1 = new Family(mother1, father1);
    private Human child1 = new Human("Nazrin", "Salimli");
    private Human child2 = new Human("Kamil", "Salimov");


    private Human mother2 = new Human("Sitara", "Mammadova");
    private Human father2 = new Human("Farman", "Mammadov");
    private Family family2 = new Family(mother2, father2);

    @Before
    public void setUp() {
        family1.addChild(child1);
        family1.addChild(child2);
    }

    public String getExpectedToString(Family family) {
        String result = "Family{mother=" + family.getMother() +
                ", father=" + family.getFather() +
                ", children=" + Arrays.toString(family.getChildren()) + ", pet=null}";
        return result;
    }

    @Test
    public void testToString() {
        Assert.assertEquals(family1.toString(), getExpectedToString(family1));
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family1.getChildren().length;
        family1.deleteChild(child2);
        Assert.assertEquals(family1.getChildren().length, resultBefore - 1);

        for (Human child : family1.getChildren()) {
            Assert.assertNotSame(child, child2);

        }
    }

    @Test
    public void testDeleteChildNegative() {
        String resultBefore = Arrays.toString(family1.getChildren());
        family1.deleteChild(new Human("Nazrin", "Salimli"));
        Assert.assertNotEquals(resultBefore, Arrays.toString(family1.getChildren()));
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family1.getChildren().length;
        family1.deleteChild(1);
        Assert.assertEquals(family1.getChildren().length, resultBefore - 1);

        for (Human child : family1.getChildren()) {
            Assert.assertNotSame(child, child2);
        }
        Assert.assertEquals(Arrays.toString(family1.getChildren()), "[" + child1.toString() + "]");
    }

    @Test
    public void testDeleteChildIndexNegative() {
        int resultBefore = family1.getChildren().length;
        int index = 0;
        if (index > family1.getChildren().length - 1) {
            family1.deleteChild(index);
            Assert.assertEquals(family1.getChildren().length, resultBefore - 1);

            for (Human child : family1.getChildren()) {
                Assert.assertNotSame(child, child2);
            }
            Assert.assertEquals(Arrays.toString(family1.getChildren()), "[" + child1.toString() + "]");
        }
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family1.getChildren().length;
        Human newChild = new Human("Leyli", "Salimli");
        family1.addChild(newChild);
        Assert.assertEquals(family1.getChildren().length, resultBefore + 1);
        Assert.assertEquals(family1.getChildren()[family1.getChildren().length - 1], newChild);
    }

    @Test
    public void testCountFamily() {
        int resultCount = family2.countFamily();
        family2.addChild(new Human());
        Assert.assertEquals(++resultCount, family2.countFamily());
        family2.addChild(new Human());
        Assert.assertEquals(++resultCount, family2.countFamily());
    }
}




