import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;


public class HumanTest {
    private Human human = new Human("Saida", "Salimova", 1983, 100, new String[][]{{String.valueOf(DayOfWeek.MONDAY)}, {"Just go sleep"}});
    private Human human1 = new Human("Mammad", "Salimov", 1987, 100, new String[][]{{String.valueOf(DayOfWeek.SUNDAY)}, {"Meet with friends"}});

    @Before
    public String getExpectedToString(Human human) {
        String result = "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", weekNotes=" + Arrays.deepToString(human.getWeekNotes()) + "}";
        return result;
    }

    @Test
    public void testToString() {
        Assert.assertEquals(human.toString(), getExpectedToString(human));
    }
}