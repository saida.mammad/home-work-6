public class Main {
    public static void main(String[] args) {


        Human human1 = new Human("Saida", "Salimova");
        Human human2 = new Human("Mammad", "Salimov");


        Family family = new Family(human1, human2);
        String[][] humanWeekNotes = {
                {DayOfWeek.SUNDAY.name(), "Meet with friends"},
                {DayOfWeek.MONDAY.name(), "Just go sleep"}
        };

        human1.setWeekNotes(humanWeekNotes);
        human2.setWeekNotes(humanWeekNotes);

        family.addChild(new Human("Nazrin", "Salimli"));

        System.out.println(family);

        Pet pet1 = new Pet(Species.DOG, "Vafle");
        Pet pet2 = new Pet(Species.CAT, "Snow");

        System.out.println(pet1);
        System.out.println(pet2);
    }
}
